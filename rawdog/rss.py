# -*- coding: utf-8 -*-
#
# rawdog plugin to generate RSS, OPML and FOAF output
# from https://cgit.kde.org/websites/planet-kde-org.git/tree/planetkde/plugins/rss.py
# and http://offog.org/git/rawdog-plugins/rss.py
#
# Copyright 2008 Jonathan Riddell
# Copyright 2009 Adam Sampson <ats@offog.org>
# Copyright 2009 Kurt McKee <contactme@kurtmckee.org>
#
# rawdog_rss is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# rawdog_rss is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with rawdog_rss; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
# You can also visit http://www.gnu.org/ .
#
# ---
#
# This plugin supports the following configuration options:
#
# outputxml         RSS output filename
# xmltitle          Feed title (e.g. "Planet Foo")
# xmllink           Feed link (e.g. "http://planet-foo.example.com/")
# xmllanguage       Feed language (e.g. "en")
# xmlurl            URL of the generated RSS (e.g. "http://planet-foo.example.com/rss20.xml")
# xmldescription    Feed description (e.g. "People who work on foo")
# xmlownername      Feed owner's name
# xmlowneremail     Feed owner's email address
# xmlmaxarticles    Maximum number of articles to include in the feed
#                   (defaults to maxarticles if not specified)
#
# If you're using rawdog to produce a planet page, you'll probably want to have
# "sortbyfeeddate true" in your config file too.

import html.entities
import libxml2

from time import gmtime
from xml.sax.saxutils import escape, unescape

from .rawdog import detail_to_html, string_to_html

# Prepare dictionary of conversions for unescape().
# These conversions allow people to use HTML character entities such as
# &aacute; in define_name if they don't know how to input Unicode characters.
# HACK: The unicode characters currently must be re-encoded as utf-8.
htmlchars = {}
for k, v in html.entities.name2codepoint.items():
    htmlchars['&%s;' % k] = chr(v)


def rfc822_date(tm):
    """Format a GMT timestamp as returned by time.gmtime() in RFC822 format.
    (This is insensitive to the current locale.)"""
    days = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
    months = [
        "Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
        ]
    return "%s, %02d %s %04d %02d:%02d:%02d GMT" % \
        (days[tm[6]], tm[2], months[tm[1] - 1], tm[0], tm[3], tm[4], tm[5])


class RSSFeed:
    def feed_name(self, feed, config):
        """Return the label used for a feed. If it has a "name" define, use
        that; otherwise, use the feed title."""

        if "define_name" in feed.args:
            return unescape(feed.args["define_name"], htmlchars)
        else:
            return feed.get_html_name(config)

    def article_to_xml(self, xml_article, rawdog, config, article):
        entry_info = article.entry_info

        id = entry_info.get("id", config["xmlurl"] + "#id" + article.hash)
        guid = xml_article.newChild(None, 'guid', string_to_html(id, config))
        guid.setProp('isPermaLink', 'false')

        title = escape(self.feed_name(rawdog.feeds[article.feed], config))
        title = detail_to_html(entry_info.get("title_detail"), True, config)
        xml_article.newChild(None, 'title', title)

        author = escape(self.feed_name(rawdog.feeds[article.feed], config))
        xml_article.newChild(None, 'author', author)

        if article.date is not None:
            date = rfc822_date(gmtime(article.date))
            xml_article.newChild(None, 'pubDate', date)

        s = entry_info.get("link")
        if s is not None and s != "":
            xml_article.newChild(None, 'link', string_to_html(s, config))

        for key in ["content", "summary_detail"]:
            s = detail_to_html(entry_info.get(key), False, config)
            if s is not None:
                xml_article.newChild(None, 'description', escape(s))
                break

        return True

    def write_rss(self, rawdog, config, articles):
        doc = libxml2.newDoc("1.0")

        rss = doc.newChild(None, 'rss', None)
        rss.setProp('version', "2.0")
        rss.setProp('xmlns:dc', "http://purl.org/dc/elements/1.1/")
        rss.setProp('xmlns:atom', 'http://www.w3.org/2005/Atom')

        channel = rss.newChild(None, 'channel', None)
        channel.newChild(None, 'title', escape(config["xmltitle"]))
        channel.newChild(None, 'link', escape(config["xmllink"]))
        channel.newChild(None, 'language', escape(config["xmllanguage"]))
        channel.newChild(None, 'description', escape(config["xmldescription"]))

        atom_link = channel.newChild(None, 'atom:link', None)
        atom_link.setProp('href', config["xmlurl"])
        atom_link.setProp('rel', 'self')
        atom_link.setProp('type', 'application/rss+xml')

        try:
            maxarticles = int(config["xmlmaxarticles"])
        except ValueError:
            maxarticles = len(articles)
        for article in articles[:maxarticles]:
            #Planet KDE addition, don't include articles in a feedclass
            feed = rawdog.feeds[article.feed]
            itembits = {}
            toAdd = True;
            for name, value in list(feed.args.items()):
                if name.startswith("define_"):
                    itembits[name[7:]] = value
                    if "feedclass" in itembits:
                        toAdd = False
                        #ervin thinks we should have project news in the feed
                        if itembits["feedclass"] == "news":
                            toAdd = True

            if toAdd:
                xml_article = channel.newChild(None, 'item', None)
                self.article_to_xml(xml_article, rawdog, config, article)

        doc.saveFormatFile(config["outputxml"], 1)
        doc.freeDoc()

    def output_write(self, rawdog, config, articles):
        self.write_rss(rawdog, config, articles)

        return True
